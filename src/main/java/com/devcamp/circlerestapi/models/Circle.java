package com.devcamp.circlerestapi.models;

public class Circle {
    private double radius = 1.0;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return this.radius*this.radius*Math.PI;
    }


    public double getCircumference() {
        return 2*this.radius*Math.PI;
    }
    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

}
