package com.devcamp.circlerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlerestapi.models.Circle;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CircleRestAPIcontroller {
    @GetMapping("/circle") 
    public Circle createCircle (@RequestParam(required = true, name = "radius") double requeRadius) {
        Circle circle = new Circle(requeRadius);
        System.out.println(circle);
        return circle;
    }
}
