package com.devcamp.circlerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CirclerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CirclerestapiApplication.class, args);
	}

}
